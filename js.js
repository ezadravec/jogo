var barraAltura, barraLargura;
var jogadorPosicaoX, velocidadeJogador, pontosJogador;
var bolaDiametro, bolaPosX, bolaPosY, vecolidadeBola;
var velocidadeJogo, colisao;

function iniciar() {

	velocidadeJogo = 10;

	barraAltura = 15;
	barraLargura = 90;

	jogadorPosicaoX = (canvas.height - barraLargura) / 2;
	velocidadeJogador = 20;
	pontosJogador = 0;


	bolaDiametro = 10;
	bolaPosX = canvas.width / 2;
	bolaPosY = -10;
	vecolidadeBola = 5;

	canvas = document.getElementById('canvas');
	context = canvas.getContext('2d');

	document.addEventListener('keydown', keyDown);

	setInterval(gameLoop, velocidadeJogo);

}

function keyDown(e) {

	if (e.keyCode == 37 && jogadorPosicaoX > 0) {
		jogadorPosicaoX -= velocidadeJogador;
	}

	if (e.keyCode == 39 && (jogadorPosicaoX < (canvas.width - barraLargura)) ) {
		jogadorPosicaoX += velocidadeJogador;
	}

}

function gameLoop() {
	
	moveBola();

	context.clearRect(0, 0, canvas.width, canvas.height);

	context.fillRect(jogadorPosicaoX, canvas.height - barraAltura, barraLargura, barraAltura);

	context.beginPath();
	context.arc(bolaPosX, bolaPosY, bolaDiametro, 0, Math.PI * 2, true);
	context.fill();

	if((bolaPosX > jogadorPosicaoX && bolaPosX < jogadorPosicaoX + barraLargura) && bolaPosY >= canvas.height - barraAltura)
	{
	    pontosJogador++;
	    resetaBola();
	} 
	 
	context.font = "32pt Tahoma";
	context.fillText(pontosJogador, canvas.width - 70, 50);

}

function moveBola() {
	if (bolaPosY <= canvas.height){
		bolaPosY += vecolidadeBola;
	} else {
		resetaBola();
	}
}

function resetaBola() {
	bolaPosY = -10;
	bolaPosX = Math.random() * (canvas.width - 0) + 0;
}